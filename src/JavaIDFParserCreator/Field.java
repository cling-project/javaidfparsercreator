/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package JavaIDFParserCreator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author eugenio
 */
public class Field {

    private String field_id;
    private String name;
    private String variable;
    private String variable_upper_case;
    private boolean required_field = false;
    private boolean deprecated = false;
    private boolean autosizable = false;
    private boolean autocalculatable = false;
    private boolean retaincase = false;
    private boolean begin_extensible = false;
    private String type;
    private String defaultValue;
    private String units;
    private String ip_units;
    private String minimum;
    private String maximum;
    private String minimum_or_greater;
    private String maximum_or_smaller;
    private String unitsBasedOnField;
    private String external_list;
    private final Set<String> keys = new HashSet<>();
    private final ArrayList<String> notes = new ArrayList<>();
    private final ArrayList<String> object_list = new ArrayList<>();
    private final ArrayList<String> references = new ArrayList<>();
    private final ArrayList<String> reference_class_names = new ArrayList<>();

    public void setField_id(String s) {
        field_id = s.replace(" ", "").replace(",", "").replace(";", "");
    }

    public void setNameAndVariable(String line) {
        name = line;
        variable = field_id;
        variable_upper_case = variable;
        variable = Element.lowerCaseFirstLetter(variable);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getVariable_upper_case() {
        return variable_upper_case;
    }

    public void setRequired_field(boolean required_field) {
        this.required_field = required_field;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }

    public void setMinimum_or_greater(String minimum_or_greater) {
        this.minimum_or_greater = minimum_or_greater;
    }

    public void setMaximum_or_smaller(String maximum_or_smaller) {
        this.maximum_or_smaller = maximum_or_smaller;
    }

    public void setDeprecated(boolean deprecated) {
        this.deprecated = deprecated;
    }

    public void setAutosizable(boolean autosizable) {
        this.autosizable = autosizable;
    }

    public void setAutocalculatable(boolean autocalculatable) {
        this.autocalculatable = autocalculatable;
    }

    public void setRetaincase(boolean retaincase) {
        this.retaincase = retaincase;
    }

    public String getField_id() {
        return field_id;
    }

    public String getName() {
        return name;
    }

    public String getVariable() {
        return variable;
    }

    public boolean isRequired_field() {
        return required_field;
    }

    public String getType() {
        return type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getUnits() {
        return units;
    }

    public String getMinimum() {
        return minimum;
    }

    public String getMaximum() {
        return maximum;
    }

    public String getMinimum_or_greater() {
        return minimum_or_greater;
    }

    public String getMaximum_or_smaller() {
        return maximum_or_smaller;
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    public boolean isAutosizable() {
        return autosizable;
    }

    public boolean isAutocalculatable() {
        return autocalculatable;
    }

    public boolean isRetaincase() {
        return retaincase;
    }

    public Set<String> getKeys() {
        return keys;
    }

    public ArrayList<String> getNotes() {
        return notes;
    }

    public ArrayList<String> getObject_list() {
        return object_list;
    }

    public String getUnitsBasedOnField() {
        return unitsBasedOnField;
    }

    public void setUnitsBasedOnField(String unitsBasedOnField) {
        this.unitsBasedOnField = unitsBasedOnField;
    }

    public String getIp_units() {
        return ip_units;
    }

    public void setIp_units(String ip_units) {
        this.ip_units = ip_units;
    }

    public String getExternal_list() {
        return external_list;
    }

    public void setExternal_list(String external_list) {
        this.external_list = external_list;
    }

    public ArrayList<String> getReference_class_names() {
        return reference_class_names;
    }

    public ArrayList<String> getReferences() {
        return references;
    }

    public boolean isBegin_extensible() {
        return begin_extensible;
    }

    public void setBegin_extensible(boolean begin_extensible) {
        this.begin_extensible = begin_extensible;
    }

}
