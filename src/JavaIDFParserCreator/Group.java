/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package JavaIDFParserCreator;

import static JavaIDFParserCreator.Element.lowerCaseFirstLetter;
import java.util.ArrayList;

/**
 *
 * @author eugenio
 */
public class Group {
    
    private String name;
    private String directory;
    private String variableName;
    
    private ArrayList<Element> elements = new ArrayList<>();

    public void setName(String name) {
        this.name = name;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public void setElements(ArrayList<Element> elements) {
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public String getDirectory() {
        return directory;
    }

    public String getVariableName() {
        return variableName;
    }
    
    public ArrayList<Element> getElements() {
        return elements;
    }

    public void setGroupDirectoryAndName(String line) {
        name = line.replace("\\group ", "");
        directory = name.replace("/", "_");
        directory = directory.replace(" ", "_");
        directory = directory.replace("-", "_");
        directory = directory.replace(",", "");
        directory = directory.replace("(", "");
        directory = directory.replace(")", "");
        variableName = lowerCaseFirstLetter(directory.replace("_", ""));
    }
    
    
}
