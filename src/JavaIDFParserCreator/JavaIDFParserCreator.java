/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package JavaIDFParserCreator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This app creates a Java EnergyPlus Input Data File parser package from the
 * EnergyPlus Input Data Dictionary file.
 *
 * @author eugenio
 */
public class JavaIDFParserCreator implements Runnable {

    public final static String APP_NAME = "JavaIDFParserCreator";
    public final static String APP_VERSION = "v1.8.4";
    
    private final String PATH_TO_ENERGYPLUS_IDD;
    private final String PATH_TO_GROUP;
    private final String PACKAGE_NAME;
    private final boolean CORRECT_ENERGYPLUS_IDD;
    private final boolean ADD_IDD_OBJECT_AS_COMMENT;
    private final String NUMERIC_PRECISION;
    private final String NUMBER_OF_DIGITS_X;
    private final String NUMBER_OF_DIGITS_Y;
    private final String NUMBER_OF_DIGITS_Z;
    public String IDD_VERSION;
    public String IDD_BUILD;

    private final StringBuilder idfParser_imports = new StringBuilder();
    private final StringBuilder idfParser_sets = new StringBuilder();
    private final StringBuilder idfParser_hs = new StringBuilder();
    private final StringBuilder idfParser_ifs = new StringBuilder();

    /**
     * Initializes the app.
     *
     * @param args args[0] is the path to the energyplus IDD file.args[1] is the
     * path to folder where the files will be saved. args[2] is the package
     * name. args[3] true/false for correcting missing IDD data. args[4] adds
     * IDD object as a comment in the end of the file. args[5] numeric precision Float or Double.
     */
    public JavaIDFParserCreator(String[] args) {
        System.out.println("-------------------------------------------------------------");
        System.out.println("  ### " + APP_NAME + " " + APP_VERSION + " ###");
        System.out.println("""
                              2022 Creative Commons CC BY-NC-SA - Eugénio Rodrigues"
                           
                              This app was developed under CLING Project, which was
                              supported by the Foundation for Science and Technology 
                              (grant number PTDC/EME-REN/3460/2021).""");
        System.out.println("-------------------------------------------------------------");
        this.PATH_TO_ENERGYPLUS_IDD = args[0];
        this.PATH_TO_GROUP = args[1] + "/";
        this.PACKAGE_NAME = args[2];
        this.CORRECT_ENERGYPLUS_IDD = Boolean.parseBoolean(args[3]);
        this.ADD_IDD_OBJECT_AS_COMMENT = Boolean.parseBoolean(args[4]);
        this.NUMERIC_PRECISION = args[5];
        this.NUMBER_OF_DIGITS_X = args[6];
        this.NUMBER_OF_DIGITS_Y = args[7];
        this.NUMBER_OF_DIGITS_Z = args[8];
    }

    @Override
    public void run() {
        System.out.println(" Start.");
        // Groups strings into object groups
        ArrayList<String> rawLines = getFileLines();
        System.out.println(" IDD file read.");
        
        // Remove double comment lines
        removeDoubleSingleCommentLines(rawLines, "!");

        // Remove comments
        removeCommentLines(rawLines);

        // Remove undesired break lines
        removeUndesiredBreakLines(rawLines);

        // Remove double break lines
        removeDoubleBreakLines(rawLines);
        System.out.println(" IDD data preprocessed.");

        // Split lines into groups of elements
        ArrayList<Group> groups = getGroups(rawLines);

        // Remove last empty elements
        removeEmptyElements(groups);
        System.out.println(" IDD groups created.");

        // Creates directories for each group of elements
        createDirectoriesAndFiles(groups);

        // Creates the Parser file
        createParseFile();
        createPoint3DFile();
        createIDFObjectFile();
        createIDFParserFile();
        System.out.println("\t Supporting classes created.");

        System.out.println(" JAVA package created.");
        System.out.println(" End.");
    }

    private ArrayList<Group> getGroups(ArrayList<String> rawLines) {
        ArrayList<Group> groups = new ArrayList<>();
        for (int i = 0; i < rawLines.size(); i++) {
            // Creates Groups
            String line = rawLines.get(i);
            if (line.contains("\\group")) {
                Group group = new Group();
                group.setGroupDirectoryAndName(line);
                groups.add(group);
            } else {
                if (!groups.isEmpty()) {
                    Group group = groups.get(groups.size() - 1);
                    if (line.equals("")) {
                        Element element = new Element(IDD_VERSION, IDD_BUILD, PACKAGE_NAME, CORRECT_ENERGYPLUS_IDD, ADD_IDD_OBJECT_AS_COMMENT, NUMERIC_PRECISION);
                        group.getElements().add(element);
                    } else {
                        if (!group.getElements().isEmpty()) {
                            Element element = group.getElements().get(group.getElements().size() - 1);
                            if (element.getName() == null) {
                                element.setFileAndName(line);
                            }
                            element.getData().add(line);
                        }
                    }
                }
            }
        }
        return groups;
    }
    
    private void removeDoubleSingleCommentLines(ArrayList<String> rawLines, String s) {
        for (int i = rawLines.size() - 1; i > 0; i--) {
            if (i > 1) {
                String currentLine = rawLines.get(i);
                String previousLine = rawLines.get(i - 1);
                if (currentLine.equals(s) && previousLine.equals(s)) {
                    rawLines.set(i, "");
                }
            }
        }
    }

    private void removeCommentLines(ArrayList<String> rawLines) {
        for (int i = rawLines.size() - 1; i > 0; i--) {
            String currentLine;
            boolean remove = false;
            if (rawLines.get(i).length() > 0) {
                currentLine = rawLines.get(i).substring(0, 1);
                if (currentLine.equals("!")) {
                    remove = true;
                }
            }
            if (rawLines.get(i).length() > 1) {
                currentLine = rawLines.get(i).substring(0, 2);
                if (currentLine.equals(" !")) {
                    remove = true;
                }
            }
            if (rawLines.get(i).length() > 7) {
                currentLine = rawLines.get(i).substring(0, 8);
                if (currentLine.equals("       !")) {
                    remove = true;
                }
            }
            if (remove) {
                rawLines.remove(i);
            }
        }
    }

    private void removeDoubleBreakLines(ArrayList<String> rawLines) {
        for (int i = rawLines.size() - 1; i > 0; i--) {
            if (i > 1) {
                String currentLine = rawLines.get(i);
                String previousLine = rawLines.get(i - 1);
                if (currentLine.equals("") && previousLine.equals("")) {
                    rawLines.remove(i);
                }
            }
        }
    }

    private void removeUndesiredBreakLines(ArrayList<String> rawLines) {
        for (int i = rawLines.size() - 2; i > 1; i--) {
            if (i > 1) {
                String currentLine = rawLines.get(i);
                String previousLine = rawLines.get(i - 1);
                String nextLine = rawLines.get(i + 1);
                if (currentLine.equals("") && previousLine.contains("\\note fields as indicated") && nextLine.contains("\\note fields as indicated")) {
                    rawLines.remove(i);
                }
            }
        }
    }

    private ArrayList<String> getFileLines() {
        ArrayList<String> rawLines = new ArrayList<>();
        // Read IDD file
        File myObj = new File(PATH_TO_ENERGYPLUS_IDD);
        try ( Scanner myReader = new Scanner(myObj)) {
            int lineCount = 0;
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                rawLines.add(line);
                lineCount++;
                if (line.contains("!IDD_Version")) {
                    IDD_VERSION = line.split("!IDD_Version ")[1];
                }
                if (line.contains("!IDD_BUILD ")) {
                    IDD_BUILD = line.split("!IDD_BUILD ")[1];
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println(" ** ERROR ** Cannot access the file: " + ex);
        }

        return rawLines;
    }

    private void createDirectoriesAndFiles(ArrayList<Group> groups) {
        idfParser_imports.append("import java.util.ArrayList;\n");
        idfParser_imports.append("import java.util.HashSet;\n");
        idfParser_imports.append("import java.util.Set;\n");
        idfParser_imports.append("import java.util.Locale;\n");
        
        for (Group group : groups) {
            
            // Creates the group directory
            System.out.println("\tGroup " + group.getName() + " created.");
            File directory = new File(PATH_TO_GROUP + group.getDirectory());
            directory.mkdir();
            
            idfParser_sets.append("        Set<String> %s = new HashSet<>();\n".formatted(group.getVariableName()));
            
            
            idfParser_hs.append("        if(!%s.isEmpty()) {\n".formatted(group.getVariableName()));
            idfParser_hs.append("            sb.append(\n%s".formatted("""
                                                                                           \"\"\"
                                                                                        
                                                                                           !- ################################################################################################
                                                                                           !-   %s
                                                                                           !- ################################################################################################
                                                                                           \"\"\");
                                                                       """.formatted(group.getName())));
            idfParser_hs.append("            for(String s : %s) {\n".formatted(group.getVariableName()));
            idfParser_hs.append("                sb.append(s);\n");
            idfParser_hs.append("            }\n");
            idfParser_hs.append("        }\n");
            
            // Adds the elements
            for (Element element : group.getElements()) {
                FileWriter file;
                try {
                    file = new FileWriter(PATH_TO_GROUP + group.getDirectory() + "/" + element.getFile() + ".java");
                    file.write(element.getJava(group.getDirectory()).toString());
                    file.close();
                    idfParser_imports.append("import ");
                    idfParser_imports.append(PACKAGE_NAME);
                    idfParser_imports.append(".");
                    idfParser_imports.append(group.getDirectory());
                    idfParser_imports.append(".");
                    idfParser_imports.append(element.getFile());
                    idfParser_imports.append(";\n");
                    
                    idfParser_ifs.append("            if (o instanceof %s v) {\n".formatted(element.getFile()));
                    idfParser_ifs.append("                %s.add(v.parseToIDF());\n".formatted(group.getVariableName()));
                    idfParser_ifs.append("            }\n");
                    
                } catch (IOException ex) {
                    System.err.println(" ** ERROR ** Cannot access the file: " + ex);
                }
                System.out.println("\t\tObject " + element.getName() + " created.");
            }
        }
        idfParser_imports.append("import java.io.FileWriter;\n");
        idfParser_imports.append("import java.io.IOException;\n");
    }

    private void removeEmptyElements(ArrayList<Group> groups) {
        for (Group group : groups) {
            Element lastElement = group.getElements().get(group.getElements().size() - 1);
            if (lastElement.getName() == null) {
                group.getElements().remove(lastElement);
            }
        }
    }

    private void createParseFile() {
        String java = """
                      public class Parse {
                      
                          private static final String BLANK = "";
                          private static final String AUTOCALCULATE = "Autocalculate";
                          private static final String AUTOSIZE = "Autosize";
                      
                          public static Object value(Object value) {
                              if (value == null) {
                                  return BLANK;
                              }
                              if (value instanceof Boolean b) {
                                  if(b) {
                                      return "Yes";
                                  } else {
                                      return "No";
                                  }
                              }
                              if (value instanceof Integer i) {
                                  if (i == Integer.MIN_VALUE) {
                                      return AUTOSIZE;
                                  }
                                  if (i == Integer.MAX_VALUE) {
                                      return AUTOCALCULATE;
                                  }
                                  return i;
                              }
                              if (value instanceof Float f) {
                                  if (f == Float.NEGATIVE_INFINITY) {
                                      return AUTOSIZE;
                                  }
                                  if (f == Float.POSITIVE_INFINITY) {
                                      return AUTOCALCULATE;
                                  }
                                  return f;
                              }
                              if (value instanceof Double f) {
                                  if (f == Double.NEGATIVE_INFINITY) {
                                      return AUTOSIZE;
                                  }
                                  if (f == Double.POSITIVE_INFINITY) {
                                      return AUTOCALCULATE;
                                  }
                                  return f;
                              }
                              if (value instanceof String string) {
                                  return string;
                              }
                              if (value instanceof Point3D point) {
                                  return String.format(Locale.ROOT, "%.$digits_x$f, %.$digits_y$f, %.$digits_z$f", point.getX(), point.getY(), point.getZ());
                              }
                              return convertToOriginalSymbols(value.toString());
                          }
                      
                          public static String convertToOriginalSymbols(String s) {
                              String ns = s;
                              ns = ns.replace("$dot$", ".");
                              ns = ns.replace("$dash$", "-");
                              ns = ns.replace("$colon$", ":");
                              ns = ns.replace("$rightSlash$", "/");
                              ns = ns.replace("$zero$", "0");
                              ns = ns.replace("$one$", "1");
                              ns = ns.replace("$two$", "2");
                              ns = ns.replace("$three$", "3");
                              ns = ns.replace("$four$", "4");
                              ns = ns.replace("$five$", "5");
                              ns = ns.replace("$six$", "6");
                              ns = ns.replace("$space$", " ");
                              ns = ns.replace("$cardinal$", "#");
                              return ns;
                          }
                      
                          public static String convertToSafeSymbols(String s) {
                              String ns = s;
                              if (ns.substring(0, 1).equals(" ")) {
                                  ns = ns.substring(1, ns.length());
                              }
                              ns = ns.replace(".", "$dot$");
                              ns = ns.replace("-", "$dash$");
                              ns = ns.replace(":", "$colon$");
                              ns = ns.replace("/", "$rightSlash$");
                              ns = ns.replace("0", "$zero$");
                              ns = ns.replace("1", "$one$");
                              ns = ns.replace("2", "$two$");
                              ns = ns.replace("3", "$three$");
                              ns = ns.replace("4", "$four$");
                              ns = ns.replace("5", "$five$");
                              ns = ns.replace("6", "$six$");
                              ns = ns.replace(" ", "$space$");
                              ns = ns.replace("#", "$cardinal$");
                              return ns;
                          }
                      }
                      """;

        FileWriter file;
        try {
            file = new FileWriter(PATH_TO_GROUP + "Parse.java");
            String new_java = java.replace("$digits_x$", NUMBER_OF_DIGITS_X).replace("$digits_y$", NUMBER_OF_DIGITS_Y).replace("$digits_z$", NUMBER_OF_DIGITS_Z);
            file.write(getSupportFileHeader("import java.util.Locale;") + new_java);
            file.close();
        } catch (IOException ex) {
            System.err.println(" ** ERROR ** Cannot access the file: " + ex);
        }
    }

    private void createPoint3DFile() {
        String java = """
                      public class Point3D {
                          
                          private final float x;
                          private final float y;
                          private final float z;
                      
                          /**
                           * Sets a point in a three dimensional space.
                           * @param x x-coordinate
                           * @param y y-coordinate
                           * @param z z-coordinate
                           */
                          public Point3D(float x, float y, float z) {
                              this.x = x;
                              this.y = y;
                              this.z = z;
                          }
                      
                          /**
                           * Returns x-coordinate.
                           * @return x-coordinate
                           */
                          public float getX() {
                              return x;
                          }
                      
                          /**
                           * Returns y-coordinate.
                           * @return y-coordinate
                           */
                          public float getY() {
                              return y;
                          }
                      
                         /**
                          * Returns z-coordinate.
                          * @return z-coordinate
                          */
                          public float getZ() {
                              return z;
                          }
                      }""";
        FileWriter file;
        try {
            file = new FileWriter(PATH_TO_GROUP + "Point3D.java");
            file.write(getSupportFileHeader("") + java.replace("float", NUMERIC_PRECISION.toLowerCase()));
            file.close();
        } catch (IOException ex) {
            System.err.println(" ** ERROR ** Cannot access the file: " + ex);
        }
    }

    private void createIDFObjectFile() {
        String java = """
                      public class IDFObject {
                      
                          private final boolean unique_object;
                          private final boolean required_object;
                          private final boolean obsolete;
                      
                          public IDFObject(boolean unique_object, boolean required_object, boolean obsolete) {
                              this.unique_object = unique_object;
                              this.required_object = required_object;
                              this.obsolete = obsolete;
                          }
                      
                          public boolean isRequired_object() {
                              return required_object;
                          }
                      
                          public boolean isUnique_object() {
                              return unique_object;
                          }
                      
                          public boolean isObsolete() {
                              return obsolete;
                          }
                      }""";

        FileWriter file;
        try {
            file = new FileWriter(PATH_TO_GROUP + "IDFObject.java");
            file.write(getSupportFileHeader("") + java);
            file.close();
        } catch (IOException ex) {
            System.err.println(" ** ERROR ** Cannot access the file: " + ex);
        }
    }

    private void createIDFParserFile() {
        String java = """
                      public class IDFParser {
                      
                          private final String IDD_VERSION = "%s";
                          private final String IDD_BUILD = "%s";
                          
                          private final ArrayList<String> iDFcomments = new ArrayList<>();
                          private final ArrayList<IDFObject> iDFobjects = new ArrayList<>();
                      
                      
                          public String getIDD_VERSION() {
                              return IDD_VERSION;
                          }
                          
                          public String getIDD_BUILD() {
                              return IDD_BUILD;
                          }
                      
                          public ArrayList<String> getIDFcomments() {
                              return iDFcomments;
                          }
                      
                          public String getIDFComment(int id) {
                              return iDFcomments.get(id);
                          }
                      
                          public void setIDFComment(int id, String s) {
                              iDFcomments.set(id, s);
                          }
                      
                          public void addIDFComment(String s) {
                              iDFcomments.add(s);
                          }
                          
                          public String getIDFCommentsAsString() {
                              StringBuilder sb = new StringBuilder();
                              for(String s : iDFcomments) {
                                  sb.append(s);
                              }
                              return sb.toString();
                          }
                      
                          public ArrayList<IDFObject> getIDFobjects() {
                              return iDFobjects;
                          }
                      
                          public IDFObject getIDFObject(int id) {
                              return iDFobjects.get(id);
                          }
                          
                          public void setIDFObject(int id, IDFObject idfobject) {
                              this.iDFobjects.set(id, idfobject);
                          }
                      
                          public void addIDFObject(IDFObject idfobject) {
                              this.iDFobjects.add(idfobject);
                          }
                      
                          public IDFObject getLastIDFObject() {
                              return this.iDFobjects.get(this.iDFobjects.size() - 1);
                          }
                      
                          public int getLastIDFObjectID() {
                              return this.iDFobjects.size() - 1;
                          }
                          
                          public String parseToIDF() {
                      
                      %s
                              for(IDFObject o : iDFobjects) {
                      %s
                              }
                      
                              StringBuilder sb = new StringBuilder();
                      %s
                              return String.format(Locale.ROOT, sb.toString(), new Object[]{});
                          }
                          
                          public void saveIDFFile(String idf_file_name) {
                              FileWriter file;
                              try {
                                  file = new FileWriter(idf_file_name);
                                  file.write(getIDFCommentsAsString() + "\\n" + parseToIDF());
                                  file.close();
                              } catch (IOException ex) {
                                  System.err.println(" ** ERROR ** Cannot access the file: " + ex);
                              }
                          }
                      }""".formatted(this.IDD_VERSION, this.IDD_BUILD, idfParser_sets.toString(), idfParser_ifs.toString(), idfParser_hs.toString());

        FileWriter file;
        try {
            file = new FileWriter(PATH_TO_GROUP + "IDFParser.java");
            file.write(getSupportFileHeader(idfParser_imports.toString()) + java);
            file.close();
        } catch (IOException ex) {
            System.err.println(" ** ERROR ** Cannot access the file: " + ex);
        }
    }

    private String getSupportFileHeader(String imports) {
        return """
               /*
                * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
                * and build upon the material in any medium or format for noncommercial
                * purposes only, and only so long as attribution is given to the creator.
                * If you remix, adapt, or build upon the material, you must license the
                * modified material under identical terms.
                */
               package %s;
               
               %s
               
               /**
                * This package was automatically generated using EnergyPlus IDD %s Build %s with %s %s.
                * 
                * @author %s %s
                */
               """.formatted(new Object[] {
                   PACKAGE_NAME,
                   imports,
                   IDD_VERSION,
                   IDD_BUILD,
                   APP_NAME,
                   APP_VERSION,
                   APP_NAME,
                   APP_VERSION
               });
    }

}
