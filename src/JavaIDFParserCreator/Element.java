/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package JavaIDFParserCreator;

import java.util.ArrayList;

/**
 *
 * @author eugenio
 */
public class Element {

    private final static String INDENT = "    ";
    private final String IDD_VERSION;
    private final String IDD_BUILD;
    private final String PACKAGE_NAME;
    private final boolean CORRECT_ENERGYPLUS_IDD;
    private final boolean ADD_IDD_OBJECT_AS_COMMENT;
    private final String NUMERIC_PRECISION;

    private String name;
    private String file;
    private String variableName;
    private final ArrayList<String> data = new ArrayList<>();

    private Integer min_fields = null;
    private boolean isUnique_object = false;
    private boolean isRequired_object = false;
    private boolean isObsolete = false;
    private String format = null;
    private Integer extensible = null;
    private boolean isFieldActivated = false;

    public Element(String IDD_VERSION, String IDD_BUILD, String PACKAGE_NAME, boolean CORRECT_ENERGYPLUS_IDD, boolean ADD_IDD_OBJECT_AS_COMMENT, String NUMERIC_PRECISION) {
        this.IDD_VERSION = IDD_VERSION;
        this.IDD_BUILD = IDD_BUILD;
        this.PACKAGE_NAME = PACKAGE_NAME;
        this.CORRECT_ENERGYPLUS_IDD = CORRECT_ENERGYPLUS_IDD;
        this.ADD_IDD_OBJECT_AS_COMMENT = ADD_IDD_OBJECT_AS_COMMENT;
        this.NUMERIC_PRECISION = NUMERIC_PRECISION;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public static String getINDENT() {
        return INDENT;
    }

    public String getIDD_VERSION() {
        return IDD_VERSION;
    }

    public String getIDD_BUILD() {
        return IDD_BUILD;
    }

    public String getPACKAGE_NAME() {
        return PACKAGE_NAME;
    }

    public boolean isCORRECT_ENERGYPLUS_IDD() {
        return CORRECT_ENERGYPLUS_IDD;
    }

    public boolean isADD_IDD_OBJECT_AS_COMMENT() {
        return ADD_IDD_OBJECT_AS_COMMENT;
    }

    public String getVariableName() {
        return variableName;
    }

    public Integer getMin_fields() {
        return min_fields;
    }

    public boolean isIsUnique_object() {
        return isUnique_object;
    }

    public boolean isIsRequired_object() {
        return isRequired_object;
    }

    public boolean isIsObsolete() {
        return isObsolete;
    }

    public String getFormat() {
        return format;
    }

    public Integer getExtensible() {
        return extensible;
    }

    public boolean isIsFieldActivated() {
        return isFieldActivated;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public void setMin_fields(Integer min_fields) {
        this.min_fields = min_fields;
    }

    public void setIsUnique_object(boolean isUnique_object) {
        this.isUnique_object = isUnique_object;
    }

    public void setIsRequired_object(boolean isRequired_object) {
        this.isRequired_object = isRequired_object;
    }

    public void setIsObsolete(boolean isObsolete) {
        this.isObsolete = isObsolete;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setExtensible(Integer extensible) {
        this.extensible = extensible;
    }

    public void setIsFieldActivated(boolean isFieldActivated) {
        this.isFieldActivated = isFieldActivated;
    }

    public void setFileAndName(String line) {
        name = line.split("!")[0];
        name = name.replace(",", "");
        name = name.replace(" ", "");

        file = name.replace(":", "_");
        file = file.replace("?", "");
        file = file.replace("-", "_");
        variableName = lowerCaseFirstLetter(file);
    }

    public StringBuilder getJava(String group) {
        StringBuilder java = new StringBuilder();
        // Copyright
        java.append("""
                    /*
                     * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
                     * and build upon the material in any medium or format for noncommercial
                     * purposes only, and only so long as attribution is given to the creator.
                     * If you remix, adapt, or build upon the material, you must license the
                     * modified material under identical terms.
                     */
                    """);

        // Package
        java.append("package ").append(PACKAGE_NAME).append(".").append(group).append(";\n\n");

        ArrayList<Field> fields = new ArrayList<>();
        StringBuilder memos = new StringBuilder();
        for (String s : data) {
            String line = s.replace("\\", "/").replace(" ,", ",");

            if (CORRECT_ENERGYPLUS_IDD) {
                // This is to correct Energy+.idd errors in creating XFACE, YFACE and ZFACE related objects.
                if (file.contains("XFACE") || file.contains("YFACE") || file.contains("ZFACE")) {
                    if (line.contains("N1,")) {
                        String[] chunks = line.split(" N2,");
                        line = chunks[0].replace("N1,", "N1, /field Value");
                    }
                }
            }

            String identifier;
            identifier = "/field ";
            if (line.contains(identifier)) {
                isFieldActivated = true;
                String[] chunks = line.split(identifier);
                Field field = new Field();
                field.setField_id(chunks[0]);
                field.setNameAndVariable(chunks[1]);
                fields.add(field);
            }
            if (isFieldActivated) {
                Field field = fields.get(fields.size() - 1);

                identifier = "/note ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.getNotes().add(chunks[1].replace("<", "").replace(">", "").replace("&", "and"));
                }

                identifier = "/key ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.getKeys().add(chunks[1]);
                }

                identifier = "/reference ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.getReferences().add(chunks[1]);
                }

                identifier = "/reference-class-name ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.getReference_class_names().add(chunks[1]);
                }

                identifier = "/object-list ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.getObject_list().add(chunks[1]);
                }

                identifier = "/required-field";
                if (line.contains(identifier)) {
                    field.setRequired_field(true);
                }

                identifier = "/deprecated";
                if (line.contains(identifier)) {
                    field.setDeprecated(true);
                }

                identifier = "/autocalculatable";
                if (line.contains(identifier)) {
                    field.setAutocalculatable(true);
                }

                identifier = "/autosizable";
                if (line.contains(identifier)) {
                    field.setAutosizable(true);
                }

                identifier = "/retaincase";
                if (line.contains(identifier)) {
                    field.setRetaincase(true);
                }

                identifier = "/units ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setUnits(chunks[1]);
                }

                identifier = "/ip-units ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setIp_units(chunks[1]);
                }

                identifier = "/unitsBasedOnField ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setUnitsBasedOnField(chunks[1]);
                }

                identifier = "/minimum ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setMinimum(chunks[1]);
                }

                identifier = "/maximum ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setMaximum(chunks[1]);
                }

                identifier = "/minimum> ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setMinimum_or_greater(chunks[1]);
                }

                identifier = "/maximum< ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setMaximum_or_smaller(chunks[1]);
                }

                identifier = "/default ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setDefaultValue(chunks[1]);
                }

                identifier = "/type ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setType(chunks[1]);
                }

                identifier = "/external-list ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    field.setExternal_list(chunks[1]);
                }

                identifier = "/begin-extensible";
                if (line.contains(identifier)) {
                    field.setBegin_extensible(true);
                }

                if (CORRECT_ENERGYPLUS_IDD) {
                    // This part corrects the Energy+.idd file objets
                    // which miss to state where begins the extensible with flag
                    // begin-extensible in the field
                    if (file.equals("ComfortViewFactorAngles") && line.contains("A3,")) {
                        field.setBegin_extensible(true);
                        extensible = 2;
                    }
                    if (file.equals("Construction") && line.contains("A2,")) {
                        field.setBegin_extensible(true);
                        extensible = 1;
                    }
                    if (file.equals("MaterialProperty_PhaseChange") && line.contains("N2,")) {
                        field.setBegin_extensible(true);
                        extensible = 2;
                    }
                    if (file.equals("MaterialProperty_VariableThermalConductivity") && line.contains("N1,")) {
                        field.setBegin_extensible(true);
                        extensible = 2;
                    }
                    if (file.contains("XFACE") || file.contains("YFACE") || file.contains("ZFACE")) {
                        if (line.contains("N1,")) {
                            field.setBegin_extensible(true);
                            extensible = 1;
                        }
                    }
                    if (file.equals("AirflowNetwork_MultiZone_Component_DetailedOpening") && line.contains("N5,")) {
                        field.setBegin_extensible(true);
                        extensible = 5;
                    }
                    if (file.equals("FenestrationSurface_Detailed") && line.contains("N4,")) {
                        field.setBegin_extensible(true);
                        extensible = 3;
                    }
                    if ((file.equals("FluidProperties_Concentration") || file.equals("FluidProperties_Superheated")) && line.contains("N2,")) {
                        field.setBegin_extensible(true);
                        extensible = 1;
                    }
                    if ((file.equals("FluidProperties_Temperatures") || file.equals("FluidProperties_Saturated")) && line.contains("N1,")) {
                        field.setBegin_extensible(true);
                        extensible = 1;
                    }

                    if (file.equals("Coil_Cooling_DX_VariableSpeed") && line.contains("N16,")) {
                        field.setBegin_extensible(true);
                        extensible = 12;
                    }
                    if (file.equals("Coil_Cooling_WaterToAirHeatPump_VariableSpeedEquationFit") && line.contains("N12,")) {
                        field.setBegin_extensible(true);
                        extensible = 13;
                    }
                    if (file.equals("Coil_Heating_WaterToAirHeatPump_VariableSpeedEquationFit") && line.contains("N6,")) {
                        field.setBegin_extensible(true);
                        extensible = 12;
                    }
                    if (file.equals("Coil_WaterHeating_AirToWaterHeatPump_VariableSpeed") && line.contains("N12,")) {
                        field.setBegin_extensible(true);
                        extensible = 12;
                    }
                    if (file.equals("PlantEquipmentOperation_ComponentSetpoint") && line.contains("A2,")) {
                        field.setBegin_extensible(true);
                        extensible = 6;
                    }
                    if (file.equals("PlantEquipmentOperation_ThermalEnergyStorage") && line.contains("A4,")) {
                        field.setBegin_extensible(true);
                        extensible = 6;
                    }
                    if (file.equals("CentralHeatPumpSystem") && line.contains("A10,")) {
                        field.setBegin_extensible(true);
                        extensible = 4;
                    }
                    if (file.equals("Coil_Heating_DX_VariableSpeed") && line.contains("N12,")) {
                        field.setBegin_extensible(true);
                        extensible = 9;
                    }
                    if ((file.equals("MaterialProperty_HeatAndMoistureTransfer_SorptionIsotherm")
                            || file.equals("MaterialProperty_HeatAndMoistureTransfer_Suction")
                            || file.equals("MaterialProperty_HeatAndMoistureTransfer_Redistribution")
                            || file.equals("MaterialProperty_HeatAndMoistureTransfer_Diffusion")
                            || file.equals("MaterialProperty_HeatAndMoistureTransfer_ThermalConductivity")) && line.contains("N2,")) {
                        field.setBegin_extensible(true);
                        extensible = 2;
                    }
                    if (file.equals("ZoneThermalChimney") && line.contains("A4,")) {
                        field.setBegin_extensible(true);
                        extensible = 4;
                    }
                    if (file.equals("Coil_Heating_DX_MultiSpeed") && line.contains("N34,")) {
                        field.setBegin_extensible(true);
                        extensible = 5;
                    }
                    if ((file.equals("PlantEquipmentOperationSchemes") || file.equals("CondenserEquipmentOperationSchemes")) && line.contains("A2,")) {
                        field.setBegin_extensible(true);
                        extensible = 3;
                    }
                    if ((file.equals("PlantEquipmentOperation_CoolingLoad") || file.equals("PlantEquipmentOperation_HeatingLoad")) && line.contains("N1,")) {
                        field.setBegin_extensible(true);
                        extensible = 3;
                    }
                    if ((file.equals("AirLoopHVAC_ControllerList") || file.equals("AirLoopHVAC_OutdoorAirSystem_EquipmentList")) && line.contains("A2,")) {
                        field.setBegin_extensible(true);
                        extensible = 2;
                    }
                    if (file.equals("CoolingTowerPerformance_YorkCalc") && line.contains("N10,")) {
                        field.setBegin_extensible(true);
                        extensible = 1;
                    }
                    if (file.equals("CoolingTowerPerformance_CoolTools") && line.contains("N9,")) {
                        field.setBegin_extensible(true);
                        extensible = 1;
                    }
                }
            } else {
                identifier = "/memo ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    memos.append("\t * ").append(chunks[1].replace("<", "").replace(">", "").replace("&", "and")).append("\n");
                }

                identifier = "/min-fields ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    min_fields = Integer.parseInt(chunks[1]);
                }

                identifier = "/unique-object";
                if (line.contains(identifier)) {
                    isUnique_object = true;
                }

                identifier = "/required-object";
                if (line.contains(identifier)) {
                    isRequired_object = true;
                }

                identifier = "/obsolete";
                if (line.contains(identifier)) {
                    isObsolete = true;
                }

                identifier = "/format ";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    format = "\"" + chunks[1] + "\"";
                }

                identifier = "/extensible:";
                if (line.contains(identifier)) {
                    String[] chunks = line.split(identifier);
                    extensible = Integer.parseInt(chunks[1].split(" ")[0]);
                }
            }
        }

//        if(fields.size() >= 40 && fields.size() < 50 && extensible == null) {
//            System.err.println(" *** WARNING *** Too large number of fields: " + group + " " + file);
//        }
        // Import libraries
        java.append("import ").append(PACKAGE_NAME).append(".IDFObject;\n");
        java.append("import ").append(PACKAGE_NAME).append(".Parse;\n");

        boolean goPoint3D = (format != null && format.equals("\"vertices\"") && extensible != null && extensible == 3);
        if (goPoint3D) {
            java.append("import ").append(PACKAGE_NAME).append(".Point3D;\n");
        }
        if (extensible != null) {
            java.append("import java.util.ArrayList;\n");
        }
        java.append("\n");

        // Create class
        java.append("/**\n").append(memos.toString().replace("\t", "")).append(" * \n");
        java.append(" * This package was automatically generated using EnergyPlus IDD ").append(IDD_VERSION).append(" Build ").append(IDD_BUILD).append(" with " + JavaIDFParserCreator.APP_NAME + " " + JavaIDFParserCreator.APP_VERSION + ".\n");
        java.append(" *\n");
        java.append(" * @author " + JavaIDFParserCreator.APP_NAME + "\n */\n");
        java.append("public class ").append(file).append(" extends IDFObject {\n\n");

        // String Builders
        StringBuilder javaConstruct = new StringBuilder();
        StringBuilder javaConstructJavaDoc = new StringBuilder();
        javaConstructJavaDoc.append("\t/**\n");
        javaConstructJavaDoc.append(memos);
        javaConstructJavaDoc.append("\t *\n");

        ArrayList<String> constructInitialization = new ArrayList<>();
        constructInitialization.add("\t" + INDENT + "super(" + isUnique_object + ", " + isRequired_object + ", " + isObsolete + ");\n");
        StringBuilder constructParameters = new StringBuilder();

        StringBuilder javaData = new StringBuilder();
        StringBuilder javaDataConstruct = new StringBuilder();
        StringBuilder javaDataConstructJavaDoc = new StringBuilder();
        javaDataConstructJavaDoc.append("\t/**\n");
        javaDataConstructJavaDoc.append("\t * List of data fields that are repeated.\n");
        javaDataConstructJavaDoc.append("\t *\n");

        ArrayList<String> constructDataInitialization = new ArrayList<>();
        StringBuilder constructDataParameters = new StringBuilder();
        StringBuilder javaEnums = new StringBuilder();
        StringBuilder javaToString = new StringBuilder();
        ArrayList<String[]> toStringIDFFields = new ArrayList<>();
        StringBuilder javaToStringReturn = new StringBuilder();
        StringBuilder javaToStringShort = new StringBuilder();
        ArrayList<String[]> toStringIDFFieldsShort = new ArrayList<>();
        StringBuilder javaToStringReturnShort = new StringBuilder();
        ArrayList<String[]> toStringDataIDFFields = new ArrayList<>();
        ArrayList<String[]> toStringDataIDFFieldsShort = new ArrayList<>();
        StringBuilder javaGetters = new StringBuilder();
        StringBuilder javaDataGetters = new StringBuilder();
        StringBuilder getter;

        // Add primitives
        java.append("    private final static String NAME = \"").append(name).append("\";\n");
        getter = new StringBuilder();
        getter.append("\t" + INDENT + "return NAME;\n");
        createClass(1, "public static", "String", "getNAME()", new StringBuilder(), javaGetters, getter);

        if (min_fields != null) {
            java.append("    private static final int MIN_FIELDS = ").append(min_fields).append(";\n");
            getter = new StringBuilder();
            getter.append("\t" + INDENT + "return MIN_FIELDS;\n");
            createClass(1, "public static", "int", "getMIN_FIELDS()", new StringBuilder(), javaGetters, getter);
        }
        if (extensible != null) {
            java.append("    private static final int EXTENSIBLE = ").append(extensible).append(";\n");
            getter = new StringBuilder();
            getter.append("\t" + INDENT + "return EXTENSIBLE;\n");
            createClass(1, "public static", "int", "getEXTENSIBLE()", new StringBuilder(), javaGetters, getter);
        }
        if (format != null) {
            java.append("    private static final String FORMAT = ").append(format).append(";\n");
            getter = new StringBuilder();
            getter.append("\t" + INDENT + "return FORMAT;\n");
            createClass(1, "public static", "String", "getFORMAT()", new StringBuilder(), javaGetters, getter);
        }

        // Create fields
        int countUpToData = 0;
        boolean startExtensible = false;
        for (Field field : fields) {
            if (field.isBegin_extensible()) {
                break;
            }
            if (startExtensible) {
            } else {
                countUpToData++;
            }
        }
        int countExtendedField = 0;
        startExtensible = false;
        int count = 0;
        for (Field field : fields) {
            count++;
            if (field.isBegin_extensible()) {
                startExtensible = true;
            }
            if (startExtensible) {
                countExtendedField++;
            }
            StringBuilder notes = new StringBuilder();
            for (String n : field.getNotes()) {
                notes.append("\n\t * ").append(n);
            }
            if (field.getUnits() != null) {
                notes.append("\n\t * Units [").append(field.getUnits()).append("].");
            }
            if (field.getDefaultValue() != null) {
                notes.append("\n\t * Default ").append(field.getDefaultValue()).append(".");
            }
            if (field.getMinimum() != null) {
                notes.append("\n\t * Minimum ").append(field.getMinimum()).append(".");
            }
            if (field.getMaximum() != null) {
                notes.append("\n\t * Maximum ").append(field.getMaximum()).append(".");
            }
            if (field.getMinimum_or_greater() != null) {
                notes.append("\n\t * Minimum_or_greater ").append(field.getMinimum_or_greater()).append(".");
            }
            if (field.getMaximum_or_smaller() != null) {
                notes.append("\n\t * Maximum_or_smaller ").append(field.getMaximum_or_smaller()).append(".");
            }
            if (field.isAutocalculatable()) {
                notes.append("\n\t * Autocalculatable.");
            }
            if (field.isAutosizable()) {
                notes.append("\n\t * Autosizable.");
            }

            if (extensible == null || (countExtendedField <= extensible)) {
                if ("choice".equals(field.getType())) {
                    if (!isBoolean(field)) {
                        StringBuilder enums = new StringBuilder();
                        int i = 0;
                        for (String s : field.getKeys()) {
                            String en;
                            if (i == field.getKeys().size() - 1) {
                                en = "\t" + INDENT + convertSymbols(s) + ";\n";
                            } else {
                                en = "\t" + INDENT + convertSymbols(s) + ",\n";
                            }
                            enums.append(en);
                            i++;
                        }
                        createClass(1, "public", "enum", field.getVariable_upper_case(), new StringBuilder(), javaEnums, enums);
                    }
                }
                if (startExtensible) {
                    // Data Series
                    if (countExtendedField == 1) {
                        String primitive = goPoint3D ? "Point3D" : "Data";
                        buildFields(field, "ArrayList<" + primitive + ">", "data", notes, 1, java, new StringBuilder(), javaGetters, constructInitialization, javaConstructJavaDoc, true, constructParameters, false, new ArrayList<>(), new StringBuilder(), new ArrayList<>(), new StringBuilder());
                        if (goPoint3D) {
                            buildFields(field, "Point3D", "point", new StringBuilder(), 2, javaData, new StringBuilder(), new StringBuilder(), constructDataInitialization, new StringBuilder(), false, new StringBuilder(), false,
                                    toStringDataIDFFields, new StringBuilder(), toStringDataIDFFieldsShort, new StringBuilder());
                        }
                    }
                    if (!goPoint3D) {
                        buildFields(field,
                                getObjectType(field),
                                field.getVariable(), notes, 2, javaData, new StringBuilder(), javaDataGetters, constructDataInitialization, javaDataConstructJavaDoc, false, constructDataParameters, false,
                                toStringDataIDFFields, new StringBuilder(), toStringDataIDFFieldsShort, new StringBuilder());
                    }
                } else {
                    buildFields(field,
                            getObjectType(field),
                            field.getVariable(), notes, 1, java, new StringBuilder(), javaGetters, constructInitialization, javaConstructJavaDoc, false, constructParameters,
                            min_fields != null && min_fields < countUpToData && count <= min_fields,
                            toStringIDFFields, javaToStringReturn, toStringIDFFieldsShort, javaToStringReturnShort);
                }
            }
        }

        // Create constructor
        java.append("\n");
        javaConstructJavaDoc.append("\t */\n");
        for (String s : constructInitialization) {
            javaConstruct.append(s);
        }
        String parameters = constructParameters.toString();
        if (parameters.length() <= 2) {
            System.err.println("** WARNING ** Length of the string of the main construct parameters is too short: " + group + " " + file);
        }
        if (parameters.length() > 2) {
            parameters = parameters.substring(0, parameters.length() - 2);
            createClass(1, "public", "", file + "(" + parameters + ")", javaConstructJavaDoc, java, javaConstruct);
        }

        // Data Class
        javaData.append("\n");
        javaDataConstructJavaDoc.append("\t */\n");
        if (extensible != null) {
            for (String s : constructDataInitialization) {
                javaDataConstruct.append(s);
            }
            String parametersData = constructDataParameters.toString();
            if (parametersData.length() <= 2 && !goPoint3D) {
                System.err.println("** WARNING ** Length of the string of the daata construct parameters is too short: " + group + " " + file);
            }
            if (parametersData.length() > 2) {
                parametersData = parametersData.substring(0, parametersData.length() - 2);
                createClass(2, "public", "", "Data(" + parametersData + ")", javaDataConstructJavaDoc, javaData, javaDataConstruct);
                createClass(1, "public static", "class", "Data", new StringBuilder(), java, javaData.append(javaDataGetters));
            }
        }

        // Create getters and setters
        java.append(javaGetters);

        // Create IDF Output
        createToString("parseToIDF()", toStringIDFFields, toStringDataIDFFields, java, javaToStringReturn, javaToString);
        if (min_fields != null && min_fields < countUpToData) {
            createToString("parseToIDFShort()", toStringIDFFieldsShort, toStringDataIDFFieldsShort, java, javaToStringReturnShort, javaToStringShort);
        }

        // Create enums
        java.append(javaEnums);

        // Close class
        java.append("}\n");

        // Adds IDD object as a comment in the end of the file.
        if (ADD_IDD_OBJECT_AS_COMMENT) {
            StringBuilder idd = new StringBuilder();
            for (String s : data) {
                idd.append(" * ").append(s.replace("\\", "/")).append("\n");
            }
            java.append("\n\n/*\n");
            java.append(idd);
            java.append(" */\n");
        }
        return java;
    }

    private static void createClass(int indent, String access, String type, String name, StringBuilder doc, StringBuilder sb, StringBuilder sb_fields) {
        String multiple_indent = getIndent(indent);

        // Creates Class
        sb.append(doc.toString().replace("\t", multiple_indent));
        sb.append(multiple_indent);
        sb.append(access);
        if (!type.equals("")) {
            sb.append(" ");
            sb.append(type);
        }
        sb.append(" ");
        sb.append(name);
        sb.append(" {\n\n");
        // Class Primitives
        sb.append(sb_fields.toString().replace("\t", multiple_indent));
        // Close Class
        sb.append(multiple_indent);
        sb.append("}\n\n");
    }

    private String convertSymbols(String s) {
        String ns = s;
        if (ns.substring(0, 1).equals(" ")) {
            ns = ns.substring(1, ns.length());
        }
        ns = ns.replace(".", "$dot$");
        ns = ns.replace("-", "$dash$");
        ns = ns.replace(":", "$colon$");
        ns = ns.replace("/", "$rightSlash$");
        ns = ns.replace("0", "$zero$");
        ns = ns.replace("1", "$one$");
        ns = ns.replace("2", "$two$");
        ns = ns.replace("3", "$three$");
        ns = ns.replace("4", "$four$");
        ns = ns.replace("5", "$five$");
        ns = ns.replace("6", "$six$");
        ns = ns.replace(" ", "$space$");
        ns = ns.replace("#", "$cardinal$");
        return ns;
    }

    private void buildFields(
            Field field,
            String primitive,
            String variable,
            StringBuilder notes,
            int indent,
            StringBuilder java,
            StringBuilder javaDoc,
            StringBuilder javaGetters,
            ArrayList<String> constructInitialization,
            StringBuilder dataJavaDoc,
            boolean isData,
            StringBuilder parameters,
            boolean isShort,
            ArrayList<String[]> toStringIDFFields,
            StringBuilder javaToStringResult,
            ArrayList<String[]> toStringIDFFieldsShort,
            StringBuilder javaToStringResultShort
    ) {
        String multiple_indent = getIndent(indent);
        java.append(multiple_indent).append("private final ").append(primitive).append(" ").append(variable).append(";\n");
        StringBuilder getter = new StringBuilder();
        getter.append("\t" + INDENT + "return ").append(variable).append(";\n");
        createClass(indent, "public", primitive, "get" + variable.substring(0, 1).toUpperCase() + variable.substring(1, variable.length()) + "()", javaDoc, javaGetters, getter);
        constructInitialization.add("\t" + INDENT + "this." + variable + " = " + variable + ";\n");
        if (isData) {
            dataJavaDoc.append("\t * @param ").append(variable).append(" Repeated fields. See Data object fields for description.\n");
        } else {
            dataJavaDoc.append("\t * @param ").append(variable).append(" ").append(field.getName()).append(notes.toString().replace("..", ".")).append("\n");
        }
        parameters.append(primitive).append(" ").append(variable).append(", ");
        String fieldName = field.getName().replace("Vertex 1 X-coordinate", "Point x,y,z").replace("Outside Layer", "Layer");
        toStringIDFFields.add(new String[]{"\t" + INDENT + "sb.append(\"  %s, !- " + fieldName + " %n\");\n",
            format == null ? ".get" + field.getVariable_upper_case() + "()"
            : format.equals("\"vertices\"") ? "" : ".get" + field.getVariable_upper_case() + "()"
        });
        javaToStringResult.append("\t" + INDENT + INDENT).append("Parse.value(").append(variable).append(")").append(",\n");
        if (isShort) {
            toStringIDFFieldsShort.add(new String[]{"\t" + INDENT + "sb.append(\"  %s, !- " + fieldName + " %n\");\n", ""});
            javaToStringResultShort.append("\t" + INDENT + INDENT).append("Parse.value(").append(variable).append(")").append(",\n");
        }
    }

    private static String getIndent(int indent) {
        int i;
        i = indent;
        String multiple_indent = "";
        while (i > 0) {
            multiple_indent += INDENT;
            i--;
        }
        return multiple_indent;
    }

    private StringBuilder removeTrailingComma(StringBuilder javaToStringReturn, String trail, String newTrail) {
        String s = javaToStringReturn.toString();
        if (s.length() == 0) {
            return new StringBuilder();
        } else {
            StringBuilder nsb = new StringBuilder();
            nsb.append(s.substring(0, s.length() - trail.length())).append(newTrail);
            return nsb;
        }
    }

    private void createToString(String getName, ArrayList<String[]> toStringIDFFields, ArrayList<String[]> toStringDataIDFFields, StringBuilder java, StringBuilder javaToStringReturn, StringBuilder javaToString) {
        StringBuilder javaToStringMiddleComplete = new StringBuilder();
        javaToStringMiddleComplete.append("\t" + INDENT + "StringBuilder sb = new StringBuilder();\n");
        javaToStringMiddleComplete.append("\t" + INDENT + "sb.append(\"%n").append(name).append(", %n\");\n");

        StringBuilder tsf = new StringBuilder();
        for (int i = 0; i < toStringIDFFields.size(); i++) {
            if (i == toStringIDFFields.size() - 1) {
                if (extensible == null) {
                    tsf.append(toStringIDFFields.get(i)[0].replace(", !- ", "; !- ").replace("\t", getIndent(1)));
                } else {
                    tsf.append(toStringIDFFields.get(i)[0].replace("\t", getIndent(1)));
                }
            } else {
                tsf.append(toStringIDFFields.get(i)[0].replace("\t", getIndent(1)));
            }
        }
        javaToStringMiddleComplete.append(tsf);

        if (extensible != null) {
            StringBuilder edtsf = new StringBuilder();
            StringBuilder dtsf = new StringBuilder();
            for (int i = 0; i < toStringDataIDFFields.size(); i++) {
                if (i == toStringDataIDFFields.size() - 1) {
                    edtsf.append(toStringDataIDFFields.get(i)[0].replace(", !- ", "; !- ").replace("\t", getIndent(3)).replace("%s", "\").append(Parse.value(data.get(i)" + toStringDataIDFFields.get(i)[1] + ")).append(\""));
                } else {
                    edtsf.append(toStringDataIDFFields.get(i)[0].replace("\t", getIndent(3)).replace("%s", "\").append(Parse.value(data.get(i)" + toStringDataIDFFields.get(i)[1] + ")).append(\""));
                }
                dtsf.append(toStringDataIDFFields.get(i)[0].replace("\t", getIndent(3)).replace("%s", "\").append(Parse.value(data.get(i)" + toStringDataIDFFields.get(i)[1] + ")).append(\""));
            }
            javaToStringMiddleComplete.append("\t" + INDENT + "for (int i = 0; i < data.size(); i++) {\n");
            javaToStringMiddleComplete.append("\t" + INDENT + INDENT + "if (i == data.size() - 1 ) {\n");
            javaToStringMiddleComplete.append(edtsf);
            javaToStringMiddleComplete.append("\t" + INDENT + INDENT + "} else {\n");
            javaToStringMiddleComplete.append(dtsf);
            javaToStringMiddleComplete.append("\t" + INDENT + INDENT + "}\n");
            javaToStringMiddleComplete.append("\t" + INDENT + "}\n");
        }

        javaToStringMiddleComplete.append("\n\t" + INDENT + "return String.format(sb.toString(), new Object[]{\n");
        javaToStringMiddleComplete.append(removeTrailingComma(javaToStringReturn, ",\n", "\n"));
        javaToStringMiddleComplete.append("\t" + INDENT + "});\n");
        createClass(1, "public", "String", getName, new StringBuilder(), javaToString, javaToStringMiddleComplete);
        java.append(javaToString);
    }

    public static String lowerCaseFirstLetter(String s) {
        return s.substring(0, 1).toLowerCase() + s.substring(1, s.length());
    }

    private String getObjectType(Field field) {
        if ("choice".equals(field.getType())) {
            if (isBoolean(field)) {
                return "Boolean";
            } else {
                return field.getVariable_upper_case();
            }
        } else {
            if (field.getField_id().contains("N")) {
                if (field.getType() != null && field.getType().contains("integer")) {
                    return "Integer";
                } else {
                    return NUMERIC_PRECISION;
                }
            } else {
                return "String";
            }
        }
    }

    private boolean isBoolean(Field field) {
        boolean isBoolean = false;
        if (field.getKeys().size() == 2) {
            int countBooleans = 0;
            for (String s : field.getKeys()) {
                String ss = convertSymbols(s);
                boolean b1 = ss.contains("Yes");
                boolean b2 = ss.contains("No");
                if (b1 || b2) {
                    countBooleans++;
                }
            }
            isBoolean = countBooleans == 2;
        }
        return isBoolean;
    }
}
