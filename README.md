# Overview
The ‘JavaIDFParserCreator’ is a Java app for creating a JAVA EnergyPlus Input Data File Parser package from EnergyPlus Input Data Dictionary (IDD) file version 23.2.

# Requirements
‘JavaIDFParserCreator’ was tested in machines running ‘Java 8 Update 321’ and ‘JDK 17’ in mac OS.

# Quick start
After guaranteeing the latest version of Java and JDK, download '[JavaIDFParserCreator v1.8.4](https://www.adai.pt/cling-project/app_releases/JavaIDFParserCreator_v1.8.4.jar)'. Run the app by double clicking the file or running the command `java -jar JavaIDFParserCreator.jar` in the terminal.

# Troubleshooting
In mac OS, the content in some folders may not show when selecting a file or the output folder. This happens when we double click the app to open it. When running from the terminal, the app works fine. In order to solve this issue, the user needs to add 'JavaLauncher.app' to full access rights in the system preferences of the operating system.

You can overcome this issue by:

- Go to System Preferences -> Security -> Privacy -> Full Disk Access.

- Drag and drop 'JavaLauncher.jar' to the list. You can find it at '/System/Library/CoreServices/Jar Launcher.app' (mac OS Monterey).

# Reporting issues and suggesting enhancements
See CONTRIBUTING.md file.

# Code of conduct
See CODE_OF_CONDUCT.md file.

# Disclaimer
The software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.

# Licensing
See LICENSE.md file.

# Acknowledgments
This work is framed under the ‘[Energy for Sustainability Initiative](https://www.uc.pt/en/efs)’ of the [University of Coimbra](http://www.uc.pt). The authors are grateful to all Ph.D. students that have tested the app.

This app was developed under CLING Project, which was supported by the Foundation for Science and Technology (grant number PTDC/EME-REN/3460/2021).